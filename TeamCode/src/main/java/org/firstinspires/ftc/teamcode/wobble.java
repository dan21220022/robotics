package org.firstinspires.ftc.teamcode;

import android.graphics.Color;
import android.util.Log;
import com.qualcomm.hardware.rev.RevColorSensorV3;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.ColorSensor;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.NormalizedColorSensor;
import com.qualcomm.robotcore.hardware.NormalizedRGBA;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.hardware.SwitchableLight;
import java.io.PrintStream;
import java.util.concurrent.TimeUnit;

public class wobble implements Runnable{
    DcMotor WobbleMotor;
    Servo WobbleServo;

    float MotorPower;
    float ServoPos;

    boolean IsActive = true;

    public wobble(DcMotor wobbleMotor, Servo wobbleServo)
    {
        WobbleMotor = wobbleMotor;
        WobbleServo = wobbleServo;
    }

    public void setParams(float motorPower, float servoPos)
    {
        MotorPower = motorPower;
        ServoPos = servoPos;
    }

    private void runMotor()
    {

        //1/3 power is static
        WobbleMotor.setPower(MotorPower);
    }


    private void runServo()
    {
        WobbleServo.setPosition(ServoPos);
    }

    public void setEnd(boolean isActive)
    {
        IsActive = isActive;
    }


    public void run()
    {
        while(IsActive) {
            runMotor();
            runServo();
        }
    }
}
