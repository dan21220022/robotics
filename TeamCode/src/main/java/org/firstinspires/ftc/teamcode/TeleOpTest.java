package org.firstinspires.ftc.teamcode;

import android.graphics.Color;
import android.os.Debug;
import android.util.Log;
import com.qualcomm.hardware.rev.RevColorSensorV3;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.ColorSensor;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.NormalizedColorSensor;
import com.qualcomm.robotcore.hardware.NormalizedRGBA;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.hardware.SwitchableLight;
import java.io.PrintStream;

@TeleOp(name = "TeleOp Test", group = "tests")
public class TeleOpTest extends LinearOpMode
{
    private DcMotor FLMotor;
    private DcMotor FRMotor;
    private DcMotor BLMotor;
    private DcMotor BRMotor;
    private DcMotor FireMotor;
    private DcMotor PumpMotor;
    private DcMotor WobbleMotor;

    private RevColorSensorV3 DownClrSensor;
    private RevColorSensorV3 UpClrSensor;
    private RevColorSensorV3 WobbleSensor;

    //private DcMotor wobbleMotor;

    private Servo LeftLiftServo;
    private Servo RightLiftServo;
    private Servo FireServo;
    private Servo WobbleServo;

    @Override
    public void runOpMode() throws InterruptedException
    {
        FLMotor = hardwareMap.dcMotor.get("FLMotor");
        FRMotor = hardwareMap.dcMotor.get("FRMotor");
        BLMotor = hardwareMap.dcMotor.get("BLMotor");
        BRMotor = hardwareMap.dcMotor.get("BRMotor");
        FireMotor = hardwareMap.dcMotor.get("FireMotor");
        PumpMotor = hardwareMap.dcMotor.get("PmpMotor");
        WobbleMotor = hardwareMap.dcMotor.get("WobbleMotor");

        LeftLiftServo = hardwareMap.servo.get("LLiftServo");
        RightLiftServo = hardwareMap.servo.get("RLiftServo");
        FireServo = hardwareMap.servo.get("FireServo");
        WobbleServo = hardwareMap.servo.get("WobbleServo");

        DownClrSensor = hardwareMap.get(RevColorSensorV3.class, "colorDown");
        UpClrSensor = hardwareMap.get(RevColorSensorV3.class, "colorUp");
        telemetry.update();

        float PowerForward = 0;
        float PowerBackward = 0;
        float PowerXAxis = 0;
        float PowerTurn = 0;
        float PowerPmp = 0;
        float LLiftPos = 0;
        float RLiftPos = 1;
        float FireServoPos = 0;
        float WobblePower = 0;
        float WobblePos = 0.6f;

        DriveClass movementThread = new DriveClass(FLMotor, FRMotor, BLMotor, BRMotor);
        shootAndPump WorkingThread = new shootAndPump(PumpMotor, LeftLiftServo, RightLiftServo, FireServo, DownClrSensor, UpClrSensor);
        wobble WobbleThread = new wobble(WobbleMotor, WobbleServo);
        new Thread(movementThread).start();
        new Thread(WorkingThread).start();
        new Thread(WobbleThread).start();

        waitForStart();

        while (opModeIsActive())
        {
            PowerForward = gamepad1.right_trigger;
            PowerBackward = gamepad1.left_trigger;
            if (gamepad1.left_bumper)
            {

                PowerXAxis = -1;
            }
            else if(gamepad1.right_bumper)
            {

                PowerXAxis = 1;
            }
            else
            {
                PowerXAxis = 0;
            }
            PowerTurn = gamepad1.left_stick_x;

            if(gamepad2.a)
            {
                PowerPmp = 1f;
            }
            else if(gamepad2.b)
            {
                PowerPmp = -1f;
            }
            else
            {
                PowerPmp = 0f;
            }

            if(gamepad2.right_trigger > 0)
            {
                WobblePower = 1f/3f;
            }
            else if(gamepad2.left_trigger > 0)
            {
                WobblePower = -gamepad2.left_trigger * (10f/12f);
            }
            else
            {
                WobblePower = 0;
            }

            if(gamepad2.left_bumper)
            {
                WobblePos += 0.01f;
            }
            else if(gamepad2.right_bumper)
            {
                WobblePos -= 0.01f;
            }

            if(gamepad2.dpad_up && WorkingThread.isFull && !WorkingThread.IsShooting)
            {
                WorkingThread.fire();
            }

            if(gamepad2.x && !WorkingThread.isFull && !WorkingThread.IsShooting)
            {
                LLiftPos = 1;
                RLiftPos = 0;
                WorkingThread.setParams(PowerPmp, LLiftPos, RLiftPos, FireServoPos);
            }
            else if(gamepad2.y && !WorkingThread.isFull && !WorkingThread.IsShooting)
            {
                LLiftPos = 0;
                RLiftPos = 1;
                WorkingThread.setParams(PowerPmp, LLiftPos, RLiftPos, FireServoPos);
            }

            movementThread.setParams(PowerForward, PowerBackward, PowerXAxis, PowerTurn);
            WorkingThread.setParams(PowerPmp, LLiftPos, RLiftPos, FireServoPos);
            WobbleThread.setParams(WobblePower, WobblePos);
        }
        movementThread.setEnd(false);
        WorkingThread.setEnd(false);
        WobbleThread.setEnd(false);
    }
}
