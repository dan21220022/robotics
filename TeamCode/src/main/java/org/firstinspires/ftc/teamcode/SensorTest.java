package org.firstinspires.ftc.teamcode;

import android.graphics.Color;
import android.os.Debug;
import android.util.Log;
import com.qualcomm.hardware.rev.RevColorSensorV3;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.ColorSensor;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.NormalizedColorSensor;
import com.qualcomm.robotcore.hardware.NormalizedRGBA;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.hardware.SwitchableLight;
import java.io.PrintStream;

@TeleOp(name = "Sensor Test", group = "tests")
public class SensorTest extends LinearOpMode
{
    private DcMotor FLMotor;
    private DcMotor FRMotor;
    private DcMotor BLMotor;
    private DcMotor BRMotor;
    private DcMotor FireMotor;
    private DcMotor PumpMotor;
    private DcMotor WobbleMotor;

    private RevColorSensorV3 DownClrSensor;
    private RevColorSensorV3 UpClrSensor;
    private RevColorSensorV3 WobbleSensor;

    //private DcMotor wobbleMotor;

    private Servo LeftLiftServo;
    private Servo RightLiftServo;
    private Servo FireServo;
    private Servo WobbleServo;


    @Override
    public void runOpMode() throws InterruptedException
    {
        FLMotor = hardwareMap.dcMotor.get("FLMotor");
        FRMotor = hardwareMap.dcMotor.get("FRMotor");
        BLMotor = hardwareMap.dcMotor.get("BLMotor");
        BRMotor = hardwareMap.dcMotor.get("BRMotor");
        FireMotor = hardwareMap.dcMotor.get("FireMotor");
        PumpMotor = hardwareMap.dcMotor.get("PmpMotor");
        WobbleMotor = hardwareMap.dcMotor.get("WobbleMotor");

        LeftLiftServo = hardwareMap.servo.get("LLiftServo");
        RightLiftServo = hardwareMap.servo.get("RLiftServo");
        FireServo = hardwareMap.servo.get("FireServo");
        WobbleServo = hardwareMap.servo.get("WobbleServo");

        DownClrSensor = hardwareMap.get(RevColorSensorV3.class, "colorDown");
        UpClrSensor = hardwareMap.get(RevColorSensorV3.class, "colorUp");
        telemetry.update();
        waitForStart();

        while (opModeIsActive())
        {
            telemetry.addData("detected down" , (DownClrSensor.getLightDetected()));
            telemetry.addData("detected up" , (UpClrSensor.getLightDetected()));
            telemetry.update();
        }

    }
}
