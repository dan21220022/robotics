package org.firstinspires.ftc.teamcode;

import android.graphics.Color;
import android.os.Debug;
import android.util.Log;
import com.qualcomm.hardware.rev.RevColorSensorV3;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.ColorSensor;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.NormalizedColorSensor;
import com.qualcomm.robotcore.hardware.NormalizedRGBA;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.hardware.SwitchableLight;
import java.io.PrintStream;
import java.util.concurrent.TimeUnit;

public class shootAndPump implements Runnable{
    private DcMotor PumpMotor;
    private DcMotor FireMotor;

    private Servo LeftLiftServo;
    private Servo RightLiftServo;
    private Servo FireServo;

    private RevColorSensorV3 DownClrSensor;
    private RevColorSensorV3 UpClrSensor;

    float PumpPower = 0;
    float PosLiftL = 0;
    float PosLiftR = 0;
    float FirePos = 0;
    float PowerFire = 0;

    private boolean IsActive = true;
    public boolean isFull = false;
    public boolean IsShooting = false;


    public shootAndPump(DcMotor pmp, Servo LLift, Servo RLift, Servo fireServo, RevColorSensorV3 downClrSensor, RevColorSensorV3 upClrSensor)
    {
        PumpMotor = pmp;
        LeftLiftServo = LLift;
        RightLiftServo = RLift;
        FireServo = fireServo;
        DownClrSensor = downClrSensor;
        UpClrSensor = upClrSensor;
    }

    public void setParams(float pmpPower, float posLiftL, float posLiftR, float firePos)
    {
        PumpPower = pmpPower;
        PosLiftL = posLiftR;
        PosLiftR = posLiftL;
        FirePos = firePos;
    }

    public void Pump()
    {
        PumpMotor.setPower(PumpPower);
    }

    public void isFull() throws InterruptedException
    {
        if(DownClrSensor.getLightDetected() >= 0.9)
        {
            Thread.sleep(100);
            if(UpClrSensor.getLightDetected() >= .9)
            {
                PosLiftL = 0;
                PosLiftR = 1;
                setLiftPos();
                isFull = true;
            }
            else
            {
                isFull = false;
            }
        }
        else
        {
            isFull = false;
        }
    }

    public void setLiftPos()
    {
        LeftLiftServo.setPosition(PosLiftL);
        RightLiftServo.setPosition(PosLiftR);
    }

    public void SetFirePos()
    {
        FireServo.setPosition(FirePos);
    }

    public void setEnd(boolean isActive)
    {
        IsActive = isActive;
    }

    public void fire() throws InterruptedException
    {
        IsShooting = true;
        PowerFire = 1;
        Thread.sleep(350);
        for(int i = 0; i < 3; i++)
        {
            FirePos = 1;
            SetFirePos();
            Thread.sleep(100);
            FirePos = 0;
            SetFirePos();
            Thread.sleep(200);
        }
        IsShooting = false;
    }

    public void run()
    {
        while(IsActive)
        {
            if(!IsShooting)
            {
                if (!isFull)
                {
                    Pump();
                }

                try
                {
                    isFull();
                }
                catch (Exception e)
                {
                    //
                }
            }
        }
    }
}

